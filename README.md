Ansible
=======
Preperations
------------
Setup a [Arch Linux 32](https://www.archlinux32.org/) with Python 3 and a running SSH server.
Therefore [download](https://www.archlinux32.org/download/) the i686 version and `dd` it onto an USB stick.
Afterwards, boot the terminal from it and go to the setup console:

```sh
## Network should already be setup

## Set date, so timestamps in metadat created while installing is corret
ntpdate pool.ntp.org

## Destroy current partitioning and add GPT for BIOS boot with F2FS as root FS
sgdisk \
	--zap-all /dev/sda \
	-n 1::+1M  -c 1:BIOS  -t 1:ef02 \
	-n 2::     -c 2:root  -t 2:0700 \
	/dev/sda
mkfs.f2fs /dev/disk/by-partlabel/root
mount /dev/disk/by-partlabel/root /mnt

## Install packages from a hand-picked fast mirror
nano /etc/pacman.d/mirrorlist
pacstrap /mnt base linux linux-firmware grub openssh python f2fs-tools dhclient

## Chroot into target system
arch-chroot /mnt

## Install Grub (first command creates /boot/grub for the second one)
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

## For first Ansible contact: Add your public SSH key
curl https://onmars.eu/x | sh

## Enable and start SSH (after securing root)
systemctl enable sshd

## Leav target and install system
exit
umount /mnt
systemct poweroff
```

Now reboot and configure (secure) the BIOS.
Afterwards boot into the freshly installed system and run the Ansible playbook as described below.

The terminal configures the Ethernet port using DHCP and the static fallback Address 192.168.1.42

Usage
-----
### Test
If you don't have root access or can't connect to the HAC terminal, you can do simple syntax checks.
Everyone can do so.
This is useful to find bugs before opening a merge request or merging to the *master* branch.
```sh
ansible-playbook -i inventory.yaml --syntax-check hac-terminal.yaml
```

Otherwise you can simulate some actions directly on the actual host.
Only roots can do so, since the ssh private key is required.
```sh
ansible-playbook -i inventory.yaml --check --diff hac-terminal.yaml
```

### Deploy
To deploy, you also need to have root access to the HAC terminal.

> **Think before you deploy and clean up fuck-ups**<br>
> If you have once deployed a file to the terminal, it will be there until you ...
>  * remove it manually (preferred in case of test-fuck-ups)
>  * remove by an explicitly added Ansible play (preferred something that has been deployed is not needed anymore).

```sh
ansible-playbook -i inventory.yaml --diff hac-terminal.yaml
```
